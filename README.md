# Mòrèn (默认)
[![license](https://img.shields.io/badge/license-MIT-97CA00.svg?style=flat-square)](./LICENSE)
[![npm](https://img.shields.io/npm/v/moren.svg?style=flat-square)](https://www.npmjs.com/package/moren)
[![npm](https://img.shields.io/npm/dt/moren.svg?style=flat-square)](https://www.npmjs.com/package/moren)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/gluons/moren.svg?style=flat-square)](https://gitlab.com/gluons/moren/pipelines)
[![Snyk Vulnerabilities for npm package](https://img.shields.io/snyk/vulnerabilities/npm/moren.svg?style=flat-square)](https://snyk.io/test/npm/moren)

Fill default values in object when empty.

## Features

- Fill default values in object easily.
- Can use function to create dynamic default value.
- [TypeScript](https://www.typescriptlang.org/) declaration included.

## Installation

**Via npm:**
```bash
npm i moren
```

**Via Yarn:**
```
yarn add moren
```

## Usage

```js
const moren = require('moren');

const defaults = {
	a: 'aaa',
	b: false,
	c: obj => obj.a.length + 1
};

const completeObj = {
	a: 'XXX',
	b: true,
	c: 0
};
const incompleteObj = {
	a: 'X',
	b: true
};
const emptyObj = {};

const result1 = moren(completeObj, defaults);
/**
 * {
 *   a: 'XXX',
 *   b: true,
 *   c: 0
 * }
 */

const result2 = moren(incompleteObj, defaults);
/**
 * {
 *   a: 'X',
 *   b: true,
 *   c: 2      // It's `a.length` (1) + 1
 * }
 */

const result3 = moren(emptyObj, defaults);
/**
 * {
 *   a: 'aaa',
 *   b: false,
 *   c: 4      // It's `a.length` (3) + 1
 * }
 */
```

## API

### moren(obj, defaults)

#### obj

An object to be filled.

#### defaults

An object that contains default value or the function that return value.

If you give a function as object's value, **moren** will pass current `obj` into function.  
So, you can use given `obj` to create and return actual default value back.
