import Defaults from './types/Defaults';
import PartialDefaults from './types/PartialDefaults';

import getValue from './getValue';

export { Defaults, PartialDefaults };

/**
 * Fill default values in object when empty.
 *
 * @export
 * @template T Target object type
 * @param {T} obj An object
 * @param {Defaults<T>} defaults Default values object
 * @returns {T} Filled object
 */
export default function moren<T>(obj: T, defaults: Defaults<T> | PartialDefaults<T>): T {
	const newObj: T = Object.assign({}, obj); // Shallow clone

	// Fill plain value first
	Object.keys(defaults).forEach(key => {
		const defaultValue = defaults[key];

		if (typeof defaultValue !== 'function') {
			newObj[key] = getValue(newObj[key], defaultValue);
		}
	});
	// Then perform filling default values by function
	Object.keys(defaults).forEach(key => {
		const defaultValue = defaults[key];

		if (typeof defaultValue === 'function') {
			newObj[key] = getValue(newObj[key], defaultValue(newObj));
		}
	});

	return newObj;
}

module.exports = moren;
