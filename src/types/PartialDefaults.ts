import Defaults from './Defaults';

/**
 * Partial default values.
 *
 * (Default values that you can ignore some required properties)
 *
 * @template T Target object type
 */
type PartialDefaults<T> = Partial<Defaults<T>>;

export default PartialDefaults;
