/**
 * Default values.
 *
 * @template T Target object type
 */
type Defaults<T> = {
	readonly [P in keyof T]: T[P] | ((obj: T) => T[P]);
};

export default Defaults;
