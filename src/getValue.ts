import merge from 'deepmerge';
import nvl from 'nvl';

/**
 * Get filled value.
 *
 * @export
 * @param {any} value A value
 * @param {any} defaultValue A default value
 * @returns {any} Filled value
 */
export default function getValue(value: any, defaultValue: any): any {
	if (typeof value === 'object' && !Array.isArray(value)) {
		return merge(defaultValue, value);
	} else {
		return nvl(value, defaultValue);
	}
}
