import moren, { Defaults } from '../dist';

type SomeObj = {
	A?: string,
	B?: boolean,
	C?: number,
	D?: { x?: string, y?: string, z?: { z1?: string, z2?: string } }
};

const defaultObj: Defaults<SomeObj> = {
	A: 'aaa',
	B: false,
	C: obj => obj.A.length + 1,
	D: {
		x: 'X',
		y: 'Y',
		z: {
			z1: '1',
			z2: '2'
		}
	}
};

describe('Complete object', () => {
	it('should not fill default values in complete object', () => {
		const completeObj: SomeObj = {
			A: 'xxxxx',
			B: true,
			C: 999,
			D: {
				x: '1',
				y: '2',
				z: {
					z1: 'x',
					z2: 'y'
				}
			}
		};
		const finalObj = moren(completeObj, defaultObj);

		expect(finalObj).toEqual({
			A: 'xxxxx',
			B: true,
			C: 999,
			D: {
				x: '1',
				y: '2',
				z: {
					z1: 'x',
					z2: 'y'
				}
			}
		} as SomeObj);
	});
});

describe('Incomplete object', () => {
	it('should fill empty object with default values', () => {
		const emptyObj: SomeObj = {};
		const finalObj = moren(emptyObj, defaultObj);

		expect(finalObj).toEqual({
			A: 'aaa',
			B: false,
			C: 4,
			D: {
				x: 'X',
				y: 'Y',
				z: {
					z1: '1',
					z2: '2'
				}
			}
		} as SomeObj);
	});
	it('should fill incomplete object with default values', () => {
		const incompleteObj: SomeObj = {
			A: 'aaaaa',
			B: true,
			D: {
				x: 'd',
				z: {
					z2: 'dd'
				}
			}
		};
		const finalObj = moren(incompleteObj, defaultObj);

		expect(finalObj).toEqual({
			A: 'aaaaa',
			B: true,
			C: 6,
			D: {
				x: 'd',
				y: 'Y',
				z: {
					z1: '1',
					z2: 'dd'
				}
			}
		} as SomeObj);
	});
});
